omega-rpg (1:0.90-pa9-20) unstable; urgency=medium

  * QA upload.
  * Set Rules-Requires-Root: binary-targets
  * Salsa repository was moved to Games Team waiting for a formal adopter.

 -- Alexandre Detiste <tchet@debian.org>  Fri, 29 Nov 2024 14:19:32 +0100

omega-rpg (1:0.90-pa9-19) unstable; urgency=medium

  * QA upload.

  [ Anatoliy Gunya ]
  * fix FTBS (#1075330)

 -- Alexandre Detiste <tchet@debian.org>  Sat, 28 Sep 2024 20:53:46 +0200

omega-rpg (1:0.90-pa9-18) unstable; urgency=medium

  * QA upload.

  * Added d/gbp.conf to describe branch layout.
  * Updated vcs in d/control to Salsa.
  * Updated d/gbp.conf to enforce the use of pristine-tar.
  * Updated Standards-Version from 3.9.8 to 4.7.0.
  * Use wrap-and-sort -at for debian control files.
  * Replaced obsolete libncurses5-dev build dependency with libncurses-dev.
  * Update lintian override info format in d/omega-rpg.lintian-overrides
    on line 1-2.
  * Bump debhelper from deprecated 9 to 13.
  * Set debhelper-compat version in Build-Depends.
  * Drop unnecessary dh arguments: --parallel.
  * Added missing Type=Application to desktop file (Closes: #1031633).

 -- Petter Reinholdtsen <pere@debian.org>  Thu, 30 May 2024 07:45:26 +0200

omega-rpg (1:0.90-pa9-17) unstable; urgency=medium

  * QA upload.
  * Set maintainer to Debian QA Group. (see #963928)
  * Disable format hardening to workaround FTBFS with newer ncurses.
    (Closes: #995600)

 -- Adrian Bunk <bunk@debian.org>  Sun, 14 Nov 2021 22:57:15 +0200

omega-rpg (1:0.90-pa9-16) unstable; urgency=medium

  * Convert package to 3.0 (quilt) format.
  * Convert debian/rules to dh.
  * Bump debian/compat and Standards-Version. Closes: #817603
  * Replace menu file with a desktop entry. Closes: #737981

 -- Guus Sliepen <guus@debian.org>  Mon, 20 Jun 2016 13:54:03 +0200

omega-rpg (1:0.90-pa9-15) unstable; urgency=low

  * Fix potential crash when a manastorm or ball spell occurs at the edge of a
    map. Closes: #471919

 -- Guus Sliepen <guus@debian.org>  Sun, 01 Jun 2008 17:27:29 +0200

omega-rpg (1:0.90-pa9-14) unstable; urgency=low

  * Allow parallel builds.

 -- Guus Sliepen <guus@debian.org>  Fri, 04 Jan 2008 13:07:32 +0100

omega-rpg (1:0.90-pa9-13) unstable; urgency=low

  * Fix bitshifting in save_country() and save_level(). This probably fixes
    loss of state in restored savegames.

 -- Guus Sliepen <guus@debian.org>  Sat, 19 Aug 2006 18:11:28 +0000

omega-rpg (1:0.90-pa9-12) unstable; urgency=low

  * Fix enchanting negatively enchanted items. Closes: #246038
  * Fix debit card creation if bank has been robbed. Closes: #246117

 -- Guus Sliepen <guus@debian.org>  Tue, 27 Apr 2004 13:04:02 +0200

omega-rpg (1:0.90-pa9-11) unstable; urgency=low

  * Apply patch from Debian Security Team preventing buffer overflows in
    command line and environment variable handling.

 -- Guus Sliepen <guus@debian.org>  Fri,  7 Nov 2003 13:57:06 +0100

omega-rpg (1:0.90-pa9-10) unstable; urgency=low

  * Apply patch from Richard Braakman to fix disappearing items after
    escaping from pickup command. Closes: #199627

 -- Guus Sliepen <guus@debian.org>  Wed,  2 Jul 2003 14:41:27 +0200

omega-rpg (1:0.90-pa9-9) unstable; urgency=low

  * Don't cast result of getitem_prompt() to char prematurely. Closes: #161930

 -- Guus Sliepen <guus@debian.org>  Sun, 22 Sep 2002 22:06:01 +0200

omega-rpg (1:0.90-pa9-8) unstable; urgency=low

  * Remove highscore and log when purging. Closes: #161410

 -- Guus Sliepen <guus@debian.org>  Sun, 22 Sep 2002 15:00:03 +0200

omega-rpg (1:0.90-pa9-7) unstable; urgency=low

  * Added menu hints. Closes: #141850

 -- Guus Sliepen <guus@sliepen.warande.net>  Tue,  9 Apr 2002 21:36:17 +0200

omega-rpg (1:0.90-pa9-6) unstable; urgency=low

  * Include patch from Richard Braakman. Closes: #134371
  * Enforce correct permissions on hiscore file. Closes: #134372

 -- Guus Sliepen <guus@sliepen.warande.net>  Mon, 18 Feb 2002 13:54:31 +0100

omega-rpg (1:0.90-pa9-5) unstable; urgency=low

  * Added Richard Braakman's patches. Closes: #112851, #130539

 -- Guus Sliepen <guus@sliepen.warande.net>  Mon, 28 Jan 2002 22:01:38 +0100

omega-rpg (1:0.90-pa9-4) unstable; urgency=low

  * When resurrecting guards access maps.dat instead of city.dat.
    Closes: #131042

 -- Guus Sliepen <guus@sliepen.warande.net>  Sat, 26 Jan 2002 23:03:16 +0100

omega-rpg (1:0.90-pa9-3) unstable; urgency=low

  * Spelling fixes for description. Closes: #125203
  * Actually install lintian overrides file.

 -- Guus Sliepen <guus@sliepen.warande.net>  Fri, 21 Dec 2001 19:05:59 +0100

omega-rpg (1:0.90-pa9-2) unstable; urgency=low

  * Added entry to Debian's menu system.

 -- Guus Sliepen <guus@sliepen.warande.net>  Wed, 12 Dec 2001 23:48:11 +0100

omega-rpg (1:0.90-pa9-1) unstable; urgency=low

  * New upstream release.
  * Set WIZARD to "root". Closes: #112404
  * Added lintian overrides.

 -- Guus Sliepen <guus@sliepen.warande.net>  Sun, 16 Sep 2001 23:50:00 +0200

omega-rpg (0.90.4-1) unstable; urgency=low

  * Initial Release. Closes: #89200
  * Changed library directory to /usr/share/games/omega-rpg for FHS
    compliance, seperated highscore and log file to /var/games/omega-rpg.

 -- Guus Sliepen <guus@sliepen.warande.net>  Sun, 11 Mar 2001 22:57:08 +0100
